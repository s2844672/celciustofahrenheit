package temperatureCalculator;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;


public class TemperatureCalculator extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Calculator calculator;
	
    public void init() throws ServletException {
    	calculator = new Calculator();
    }	
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String docType =
      "<!DOCTYPE HTML>\n";
    String title = "Celsius to Fahrenheit:";
    out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>Degrees celsius: " +
                   request.getParameter("celsius") + "\n" +
                "  <P>Degrees fahrenheit: " +
                   Double.toString(calculator.calculateTemp(request.getParameter("celsius"))) +
                "</BODY></HTML>");
  }
  

}
